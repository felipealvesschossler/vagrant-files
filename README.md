<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Vagrant Files](#vagrant-files)
- [Pré-requisitos 💻](#pr%C3%A9-requisitos-)
  - [Software](#software)
  - [Hardware](#hardware)
- [Instalação 🚀](#instala%C3%A7%C3%A3o-)
- [Utilização 🤩](#utiliza%C3%A7%C3%A3o-)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Vagrant Files

Projeto criado para armazenar e testar meus ambientes em Vagrant.

Por enquanto o principal objetivo desse repo é Kubernetes. Dentro do folder `kubernetes` existem 3 clusters:

- `kubeadm-weavenet`: instalação do cluster "crua" somente com CNI e o MetalLB (possui 1 control-plane e 2 workers).

## Pré-requisitos 💻

### Software

- Vagrant: https://www.vagrantup.com/docs/installation
- VirtualBox: https://www.virtualbox.org/wiki/Downloads
- Instalar plugin para resize do disco: `vagrant plugin install vagrant-disksize`

### Hardware

- 16GB RAM ou mais
- 6 vCPU (idealmente)
- Disco: Boot das 3 instâncias de acordo com o ambiente desejado + tamanho da instalação/utilização das VMs

## Instalação 🚀

Basta entrar no folder que contém o ambiente desejado (terá um Vagrantfile lá dentro) e executar o comando:

```
vagrant up
```

## Utilização 🤩

Para os clusters Kubernetes será necessário pegar o arquivo de configuração dentro de alguma instância e passar para sua máquina local, assim tornando o kubectl e o cluster acessíveis. Para isso faça as seguintes etapas:

1. Após subir o cluster execute: `scp vagrant@192.168.56.50:/home/vagrant/.kube/config ~/.kube/vagrant-config`
   * A senha é: `vagrant`
2. Entre dentro do arquivo de configuração do seu Shell (no meu caso ZSH): `vim ~/.zshrc`
3. Adicione esse novo arquivo de configuração no KUBECONFIG dentro do arquivo de configuração: `export KUBECONFIG=$KUBECONFIG:~/.kube/vagrant-config`
4. Renomeie o contexto para vagrant: `kubectl config rename-context kubernetes-admin@kubernetes vagrant`
5. Feche seu editor e reinicie seu terminal








